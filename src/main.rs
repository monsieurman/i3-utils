use i3ipc::reply::Workspace;
use i3ipc::I3Connection;

use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "Move your i3 containers")]
enum Opt {
    Next,
    Prev
}

fn main() {
    let opt = Opt::from_args();
    let mut conn = I3Connection::connect().unwrap();

    let res = match opt {
        Opt::Next => move_container_to_next_workspace_and_follow(&mut conn),
        Opt::Prev => move_container_to_previous_workspace_and_follow(&mut conn)
    };

    res.unwrap();
}

fn move_container_to_next_workspace_and_follow(
    conn: &mut I3Connection,
) -> std::result::Result<i3ipc::reply::Command, i3ipc::MessageError> {
    let workspace = get_current_workspace(conn).expect("Could not get current workspace");
    let workspace_num = workspace.num + 1;
    move_container_to_workspace(conn, workspace_num)?;
    conn.run_command("workspace next")
}

fn move_container_to_previous_workspace_and_follow(
    conn: &mut I3Connection,
) -> std::result::Result<i3ipc::reply::Command, i3ipc::MessageError> {
    // TODO: Test not 0nth workspace
    let workspace = get_current_workspace(conn).expect("Could not get current workspace");
    let workspace_num = workspace.num - 1;
    move_container_to_workspace(conn, workspace_num)?;
    conn.run_command("workspace prev")
}

fn move_container_to_workspace(
    conn: &mut I3Connection,
    workspace_num: i32,
) -> std::result::Result<i3ipc::reply::Command, i3ipc::MessageError> {
    let cmd = format!("move container to workspace {}", workspace_num);
    conn.run_command(cmd.as_str())
}

fn get_current_workspace(
    conn: &mut I3Connection,
) -> std::result::Result<Workspace, i3ipc::MessageError> {
    let workspaces = conn.get_workspaces()?.workspaces;
    let workspace: Workspace = workspaces
        .into_iter()
        .filter(|w| w.focused)
        .nth(0)
        .expect("No active workspace"); // Should never be

    Ok(workspace)
}
