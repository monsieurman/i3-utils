# I3-Utils

This is a simple cli written in Rust using i3-ipc.

Features:
- i3-utils next *move current container to next workspace and create it if it doesn't exist*
- i3-utils prev *same for previous*

## Installation

You'll need [rustup and cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) installed.

Then simply run:

```sh
cargo install --git https://gitlab.com/monsieurman/i3-utils.git
# And use it like so
i3-utils next
```